import React from 'react';
import ReactDOM from 'react-dom';
import './style/main.scss';
import App from './app';
import firebase from 'firebase/app';
import './app/translate';

firebase.initializeApp({
  apiKey: 'AIzaSyAHq_vEQmCgw7eC-yoIOq1XFsIbBhldRg0',
  authDomain: 'animal-search-e5053.firebaseapp.com',
  databaseURL: 'https://animal-search-e5053.firebaseio.com',
  projectId: 'animal-search-e5053',
  storageBucket: 'animal-search-e5053.appspot.com',
  messagingSenderId: '469278340197',
  appId: '1:469278340197:web:5e0b5309dfa39dd2abc28c',
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
