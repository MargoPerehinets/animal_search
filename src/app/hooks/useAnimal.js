import { useCallback, useEffect, useState } from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore';

export const useAnimal = (animalId) => {
  const [animal, setAnimal] = useState({});
  const [fetching, setFetchingStatus] = useState(true);
  const [updating, setUpdatingStatus] = useState(false);

  useEffect(() => {
    if (animalId) {
      const fetchAnimal = async () => {
        try {
          setFetchingStatus(true);
          await firebase
            .firestore()
            .collection('animals')
            .doc(animalId)
            .get()
            .then((doc) => {
              if (doc.exists) {
                setAnimal(doc.data());
              } else {
                console.log('No such animal!');
              }
              setFetchingStatus(false);
            });
        } catch (err) {
          console.log('Animal fetch error', err);
        }
      };
      fetchAnimal();
    }
  }, [animalId, setAnimal, setFetchingStatus]);

  const updateAnimal = useCallback(
    (data, cb = () => {}) => {
      if (animalId) {
        const update = async () => {
          try {
            setUpdatingStatus(true);
            await firebase
              .firestore()
              .collection('animals')
              .doc(animalId)
              .update(data)
              .then(() => {
                setAnimal((oldAnimal) => ({ ...oldAnimal, ...data }));
                setUpdatingStatus(false);
                cb();
              });
          } catch (err) {
            console.log('Animal fetch error', err);
          }
        };
        update();
      }
    },
    [animalId],
  );

  return { animal, fetching, updating, updateAnimal };
};
