import cuid from 'cuid';
import firebase from 'firebase/app';
import 'firebase/storage';
import { isEmpty } from 'lodash';
import { useCallback, useEffect, useRef, useState } from 'react';

export const useAnimalImages = (animalId) => {
  const [progress, setProgress] = useState(0);
  const [uploading, setUploadStatus] = useState(false);
  const [deleteList, setImageToDelete] = useState([]);
  const [images, setImage] = useState([]);
  const uploadTask = useRef(null);

  useEffect(() => {
    const storageRef = firebase.storage().ref();
    storageRef
      .child(`${animalId}`)
      .listAll()
      .then((res) => {
        const conditionalPromises = [];

        res.items.forEach((itemRef) => {
          conditionalPromises.push(
            itemRef.getDownloadURL().then((uri) => {
              const [id, type] = itemRef.name.split('.');
              return {
                id,
                type,
                uri,
              };
            }),
          );
        });

        Promise.all(conditionalPromises).then((result) => {
          setImage((oldImages) => [...oldImages, ...result]);
        });
      })
      .catch((err) => {
        console.log('Error getting animal images', err);
      });
  }, [animalId]);

  const uploadFile = useCallback(
    (file, cb = () => {}) => {
      setUploadStatus(true);
      const storageRef = firebase.storage().ref();
      const id = cuid();
      const [_, type] = file.type.split('/');
      uploadTask.current = storageRef
        .child(`${animalId}/${id}.${type}`)
        .put(file);
      uploadTask.current.on(
        'state_changed',
        (snapshot) => {
          setProgress((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          // switch (snapshot.state) {
          // case firebase.storage.TaskState.PAUSED: // or 'paused'
          //     console.log('Upload is paused');
          //     break;
          // case firebase.storage.TaskState.RUNNING: // or 'running'
          //     console.log('Upload is running');
          //     break;
          // }
        },
        (err) => {
          console.log('Error during upload', err);
          setUploadStatus(false);
        },
        () => {
          uploadTask.current.snapshot.ref
            .getDownloadURL()
            .then((newImageUrl) => {
              setImage((oldImages) => [
                ...oldImages,
                { id, uri: newImageUrl, type },
              ]);
              setUploadStatus(false);
              cb();
            });
        },
      );
    },
    [animalId],
  );

  const deleteFile = useCallback(
    (file = {}, cb = () => {}) => {
      if (!animalId || isEmpty(file) || deleteList.includes(file.id)) return;
      setImageToDelete((list) => [...list, file.id]);
      const storageRef = firebase.storage().ref();
      storageRef
        .child(`${animalId}/${file.id}.${file.type}`)
        .delete()
        .then(() => {
          setImage((oldImages) => oldImages.filter(({ id }) => id !== file.id));
          setImageToDelete((list) => list.filter((id) => id !== file.id));
          cb();
        })
        .catch((err) => {
          console.log('Error during delete', err);
          setImageToDelete((list) => list.filter((id) => id !== file.id));
        });
    },
    [setImage, deleteList, setImageToDelete, animalId],
  );

  return {
    progress,
    uploading,
    uploadFile,
    deleteFile,
    deleteList,
    images,
  };
};
