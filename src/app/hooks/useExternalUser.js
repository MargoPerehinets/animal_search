import { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore';

export const useExternalUser = (userId) => {
  const [user, setUser] = useState({});
  const [fetching, setFetchingStatus] = useState(true);

  useEffect(() => {
    if (userId) {
      const fetchExternalUser = async () => {
        try {
          setFetchingStatus(true);
          await firebase
            .firestore()
            .collection('users')
            .doc(userId)
            .get()
            .then((doc) => {
              if (doc.exists) {
                setUser(doc.data());
              } else {
                console.log('No such user!');
              }
              setFetchingStatus(false);
            });
        } catch (err) {
          console.log('Animal fetch error', err);
        }
      };

      fetchExternalUser();
    }
  }, [userId, setUser, setFetchingStatus]);

  return { user, fetching };
};
