import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import en from './locales/en';
import ru from './locales/ru';
import ua from './locales/ua';

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: { en, ru, ua },
    fallbackLng: 'ua',
    debug: true,
    // lng: 'ua',
    // have a common namespace used around the full app
    ns: ['translations'],
    defaultNS: 'translations',

    keySeparator: false, // we use content as keys

    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
