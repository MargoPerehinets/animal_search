import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './containers/Header';
import { ProvideUser, useUser } from './context/user';
import AnimalDetailsPage from './pages/AnimalDetailsPage';
import AnimalListPage from './pages/AnimalListPage';
import AnimalUploadImagesPage from './pages/AnimalUploadImagesPage';
import CreateNewAnimalPage from './pages/CreateNewAnimalPage';
import HomePage from './pages/HomePage';
import NotAuthorizedPage from './pages/NotAuthorizedPage';
import NotFoundPage from './pages/NotFoundPage';
import ProfilePage from './pages/ProfilePage';
import { ReactComponent as Loader } from './../images/loader.svg';

const RouteWithAuth = (props) => {
  const { hasUser, fetchedUser } = useUser();
  // if(!fetchedUser) return <p>Loading</p>
  if (hasUser) {
    return <Route {...props} />;
  }
  if (!fetchedUser) {
    return (
      <section>
        <div className="loader-wrap">
          <Loader className="loader" />
        </div>
      </section>
    );
  }
  return <NotAuthorizedPage />;
};

export default () => (
  <ProvideUser>
    <Router>
      <Header />
      <Switch>
        <Route path="/animal-list/:reportType">
          <AnimalListPage />
        </Route>
        <RouteWithAuth path="/profile">
          <ProfilePage />
        </RouteWithAuth>
        <RouteWithAuth path="/create-new-animal/:reportType">
          <CreateNewAnimalPage />
        </RouteWithAuth>
        <RouteWithAuth path="/animal-upload-images/:animalId">
          <AnimalUploadImagesPage />
        </RouteWithAuth>
        <Route path="/" exact>
          <HomePage />
        </Route>
        <Route path="/animal-details/:animalId">
          <AnimalDetailsPage />
        </Route>
        <Route path="*">
          <NotFoundPage />
        </Route>
      </Switch>
    </Router>
  </ProvideUser>
);
