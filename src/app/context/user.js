import { isEmpty } from 'lodash';
import React, { createContext, useContext } from 'react';
import firebase from 'firebase';
import 'firebase/auth';
import 'firebase/firestore';

const UserContext = createContext(null);

const ConsumeUser = UserContext.Consumer;
class ProvideUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      hasUser: false,
      fetchedUser: false,
      updateUser: this.updateUser,
      fetchUser: this.fetchUser,
    };
    this.fbAuthObserver = null;
  }

  componentDidMount() {
    this.fbAuthObserver = firebase.auth().onAuthStateChanged(this.observerUser);
  }

  observerUser = async (fbUser) => {
    if (fbUser) {
      this.fetchUser(fbUser.uid);
    } else {
      this.setState({ user: {}, hasUser: false, fetchedUser: true });
    }
  };

  fetchUser = async (uid) => {
    const user = await firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .get()
      .then((doc) => (doc.exists ? doc.data() : {}));

    this.setState({ user, hasUser: !isEmpty(user), fetchedUser: true });
    Promise.resolve();
  };

  updateUser = (newUser = {}) => {
    if (isEmpty(newUser)) return;
    this.setState(
      ({ user }) => ({ user: { ...user, ...newUser } }),
      () => {
        firebase
          .firestore()
          .collection('users')
          .doc(this.state.user.uid)
          .update(newUser)
          .then(() => {})
          .catch((err) => console.log(err));
      },
    );
  };

  render = () => {
    const { children } = this.props;
    return (
      <UserContext.Provider value={this.state}>{children}</UserContext.Provider>
    );
  };
}

const useUser = () => useContext(UserContext);

export { useUser, ProvideUser, ConsumeUser };
