import React, { useRef, useState } from 'react';

export const InputPrimary = ({ label = '', onChange = () => {}, ...props }) => {
  const inputRef = useRef(null);
  const [focused, setFocus] = useState(false);
  return (
    <div className={`input-primary${focused ? '-active' : ''}`} onClick={() => inputRef?.current?.focus()}>
      <div className="input-wrap">
        <span className="label">{label}</span>
        <input
          onChange={({ target: { value } }) => onChange(value)}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          ref={inputRef}
          type="text"
          {...props}
        />
      </div>
    </div>
  );
};
