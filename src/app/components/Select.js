import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import ReactSelect from 'react-select';

export const Select = ({ label, isRequired, ...props }) => {
  const { t } = useTranslation();

  const customStyles = {
    container: (provided, state) => {
      return { ...provided };
    },
    input: (provided, state) => {
      return { ...provided };
    },
    control: (provided, state) => {
      return {
        ...provided,
        boxShadow: 'none',
        borderWidth: 2,
        borderColor: state.isFocused ? "#5f2eea" : "transparent",
        backgroundColor: state.isFocused ? '#ffffff' : '#eff0f7',
        borderRadius: 16,
        opacity: state.isDisabled ? 0.5 : 1,
        transition: 'none',
        height: 64,
        minWidth: 320
      };
    }, 
    valueContainer: (provided, state) => {
      return {
        ...provided,
        padding: '0 24px',
      };
    },
    indicatorsContainer: (provided, state) => {
      return {
        ...provided,
        padding: '12px 16px',
      };
    },
    indicatorSeparator: (provided, state) => ({}),
    menu: (provided, state) => {
      return ({
      ...provided,
      borderRadius: 16,
      overflow: 'hidden',
      backgroundColor: '#EFF0F7',
    })},
    menuList: (provided, state) => ({
      ...provided,
      padding: 0,
    }),
    option: (provided, state) => ({
      ...provided,
      color: state.isFocused ? '#000000' : "#6E7191",
      backgroundColor: state.isFocused ? "rgba(102, 146, 230, 0.33)" : 'transparent',
    }),
  };

  return (
    <div className="select">
      <span className="label">
        {isRequired && <span>*</span>}
        {label}
      </span>
      <ReactSelect
        styles={customStyles}
        placeholder={`${t('select')}...`}
        noOptionsMessage={() => t('noOptionsForNow')}
        maxMenuHeight={220}
        menuPlacement="auto"
        {...props}
      />
    </div>
  );
};
