import React from 'react';
import { useHistory } from 'react-router';
import { ReactComponent as CatDefault } from './../../images/cat-default.svg';
import { ReactComponent as DogDefault } from './../../images/dog-default.svg';

export const AnimalAvatar = ({
  id = '',
  type = 'cat',
  uri = '',
  name = '',
  ...props
}) => {
  const { push } = useHistory();

  return (
    <div
      className="animal-avatar"
      onClick={() => {
        if (id) push(`/animal-details/${id}`);
      }}>
      {name && <span>{name}</span>}
      <div className="img-wrap">
        {uri ? (
          <img src={uri} />
        ) : (
          {
            cat: <CatDefault />,
            dog: <DogDefault />,
          }[type] || null
        )}
      </div>
    </div>
  );
};
