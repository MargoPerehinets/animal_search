import React, { useRef, useState } from 'react';

export const InputSecondary = ({
  label = '',
  onBlur = () => {},
  onFocus = () => {},
  onChange = () => {},
  isRequired,
  raw = false,
  ...props
}) => {
  const inputRef = useRef(null);
  const [focused, setFocus] = useState(false);

  const handleChange = raw
    ? onChange
    : ({ target: { value } }) => onChange(value);
    
  return (
    <div className={`input-secondary${focused ? '-active' : ''}`}>
      {label && (
        <span className="label">
          {isRequired && <span>*</span>}
          {label}
        </span>
      )}
      <div className="input-wrap" onClick={() => inputRef?.current?.focus()}>
        <input
          onChange={handleChange}
          onFocus={() => {
            onFocus();
            setFocus(true)
          }}
          ref={inputRef}
          type="text"
          onBlur={() => {
            onBlur();
            setFocus(false)
          }}
          {...props}
        />
      </div>
    </div>
  );
};
