import { isEmpty } from 'lodash';
import React, { useState } from 'react';

export const HoverDropdown = ({ children, options = [] }) => {
  const [mouseOver, setMouseOver] = useState(false);
  return (
    <div
      style={{ position: 'relative', zIndex: 20 }}
      onMouseLeave={() => setMouseOver(false)}
      onMouseOver={() => setMouseOver(true)}>
      {children}
      {mouseOver && !isEmpty(options) && (
        <div style={{}} className="hover-dropdown">
          {options.map(({ key, onClick, label }) => {
            if (!onClick) {
              return <p>{label}</p>;
            }
            return (
              <button onClick={onClick} className="option" key={key}>
                {label}
              </button>
            );
          })}
        </div>
      )}
    </div>
  );
};