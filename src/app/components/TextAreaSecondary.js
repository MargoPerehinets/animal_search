import React, { useRef, useState } from 'react';

export const TextAreaSecondary = ({
  label = '',
  onChange = () => {},
  ...props
}) => {
  const textareaRef = useRef(null);
  const [focused, setFocus] = useState(false);
  return (
    <div className={`text-area-secondary${focused ? '-active' : ''}`}>
      {label && <span className="label">{label}</span>}
      <textarea
        onChange={({ target: { value } }) => onChange(value)}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        ref={textareaRef}
        type="text"
        {...props}
      />
    </div>
  );
};
