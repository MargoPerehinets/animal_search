import React, { useContext, useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { useTranslation } from 'react-i18next';
import { Portal, PortalWithState } from 'react-portal';
import { useUser } from '../context/user';
import AuthModal from './AuthModal';
import { HoverDropdown } from '../components/HoverDropdown';

export default () => {
  const { t } = useTranslation();
  const { push } = useHistory();
  const { hasUser, user } = useUser();

  return (
    <header>
      <div className="header-wrap">
        <button onClick={() => push('/')} className="logo">
          {t('animalSearch')}
        </button>
        <nav>
          {hasUser && (
            <HoverDropdown
              options={[
                {
                  key: '1',
                  onClick: () => push('/create-new-animal/missing'),
                  label: t('missing'),
                },
                {
                  key: '2',
                  onClick: () => push('/create-new-animal/found'),
                  label: t('found'),
                },
              ]}>
              <button>{t('report')}</button>
            </HoverDropdown>
          )}
          <button onClick={() => push('/animal-list/missing')}>
            {t('view')}
          </button>
          {hasUser ? (
            <button onClick={() => push(`/profile`)}>{t('profile')}</button>
          ) : (
            <PortalWithState closeOnEsc>
              {({ openPortal, closePortal, isOpen, portal }) => (
                <>
                  <button onClick={openPortal}>{t('login')}</button>
                  {portal(<AuthModal close={closePortal} />)}
                </>
              )}
            </PortalWithState>
          )}
        </nav>
      </div>
    </header>
  );
};
