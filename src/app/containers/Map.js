import ReactMapboxGl, {
  Layer,
  Feature,
  ZoomControl,
  ScaleControl,
} from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import { Marker } from 'react-mapbox-gl';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { ReactComponent as Loader } from './../../images/loader.svg';
import marker from './../../images/marker.png';
import { Cluster } from 'react-mapbox-gl';
import { accessToken } from './../config/mapbox.json';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { useHistory, useParams } from 'react-router';
import { useTranslation } from 'react-i18next';
import { animalTypes } from '../modules/animal-types';
import { Select } from '../components/Select';
import sortBy from 'lodash.sortby';
import { isEmpty } from 'lodash';
import { AnimalAvatar } from './../components/AnimalAvatar';

const Map = ReactMapboxGl({ accessToken });

export default () => {
  const [loading, setLoading] = useState(true);
  const [userLocation, setUserLocation] = useState([31.1656, 48.3794]);
  const [currentZoom, setCurrentZoom] = useState([6]);
  const [animalList, setAnimalList] = useState([]);

  const { reportType } = useParams();
  const { replace, push } = useHistory();
  const missing = reportType === 'missing';

  const { t } = useTranslation();

  const [type, setType] = useState(null);
  const [breed, setBreed] = useState(null);
  const [gender, setGender] = useState(null);
  const [color, setColor] = useState(null);
  const [sterilization, setSterilization] = useState(null);

  const [appliedFilters, applyFilters] = useState({});

  useEffect(() => {
    let query = firebase
      .firestore()
      .collection('animals')
      .where('reportType', '==', reportType);

    Object.keys(appliedFilters).forEach((filter) => {
      const { value = '' } = appliedFilters[filter] || {};
      if (value) query = query.where(filter, '==', value);
    });

    query.get().then((qs) => {
      const data = [];
      qs.forEach((doc) => data.push(doc.data()));
      setAnimalList(data);
    });
  }, [reportType, appliedFilters]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log([position?.coords?.longitude, position?.coords?.latitude]);
        setUserLocation([
          position?.coords?.longitude,
          position?.coords?.latitude,
        ]);
        setCurrentZoom([10]);
        setLoading(false);
      },
      (error) => {
        setLoading(false);
      },
    );
  }, []);

  const currentBreeds = useMemo(() => {
    return sortBy(
      type?.breeds.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentGenders = useMemo(() => {
    return sortBy(
      type?.genders.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentColors = useMemo(() => {
    return sortBy(
      type?.colors.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentSterilization = useMemo(() => {
    return (
      type?.sterilization.map((it) => ({
        value: it,
        label: it ? t('yes') : t('no'),
      })) || []
    );
  }, [type]);

  const hasFilters = useMemo(
    () =>
      Object.keys(appliedFilters).some(
        (filter) => appliedFilters[filter]?.value,
      ),
    [appliedFilters],
  );

  const clearFilters = useCallback(() => {
    applyFilters({
      type: null,
      breed: null,
      gender: null,
      color: null,
      sterilization: null,
    });
    setType(null);
    setBreed(null);
    setGender(null);
    setColor(null);
    setSterilization(null);
  }, []);

  const clusterMarker = useCallback((coordinates, pointCount, ...rest) => {
    const [lng, lat] = coordinates;
    return (
      <Marker key={`${lng}${lat}`} coordinates={coordinates}>
        <div className="map-cluster">
          <span>{pointCount}</span>
        </div>
      </Marker>
    );
  }, []);

  if (loading) {
    return (
      <div className="loader-wrap">
        <Loader className="loader" />
      </div>
    );
  }

  return (
    <div className="map-view">
      {!hasFilters ? (
        <div className="filter-list">
          <button
            className="button-secondary borderless"
            onClick={() => {
              replace(missing ? '/animal-list/found' : '/animal-list/missing');
            }}>
            {t(missing ? 'watchFound' : 'watchMissing')}
          </button>
          <Select
            // label={t('type')}
            // isDisabled
            isClearable
            isSearchable
            placeholder={t('type')}
            // isRequired
            options={animalTypes.map((it) => ({ ...it, label: t(it.value) }))}
            value={type}
            onChange={(val) => {
              setType(val);
              if (!val || type?.value !== val?.value) {
                setBreed(null);
                setGender(null);
                setColor(null);
                setSterilization(null);
              }
            }}
          />
          <Select
            placeholder={t('breed')}
            isDisabled={!type}
            isClearable
            isSearchable
            value={breed}
            options={currentBreeds}
            onChange={setBreed}
          />
          <Select
            placeholder={t('gender')}
            isDisabled={!type}
            isClearable
            isSearchable
            value={gender}
            options={currentGenders}
            onChange={setGender}
          />
          <Select
            placeholder={t('color')}
            isDisabled={!type}
            isClearable
            isSearchable
            value={color}
            options={currentColors}
            onChange={setColor}
          />
          <Select
            placeholder={t('sterilization')}
            isDisabled={!type}
            isClearable
            isSearchable
            value={sterilization}
            options={currentSterilization}
            onChange={setSterilization}
          />
          <div className="options">
            <button
              className="button-secondary"
              onClick={() => {
                applyFilters({ type, breed, gender, color, sterilization });
              }}>
              {t('apply')}
            </button>
            {Object.keys(appliedFilters).some(
              (filter) => appliedFilters[filter]?.value,
            ) && (
              <button className="button-secondary" onClick={clearFilters}>
                {t('clear')}
              </button>
            )}
          </div>
        </div>
      ) : (
        <div className="animal-list">
          <button className="button-secondary" onClick={clearFilters}>
            {t('cancel')}
          </button>
          {animalList.map(({ id, type, primaryImage, name }) => {
            return (
              <div key={id} className="animal-list-item">
                <AnimalAvatar
                  id={id}
                  type={type}
                  name={name}
                  uri={primaryImage.uri}
                />
              </div>
            );
          })}
        </div>
      )}
      <Map
        style="mapbox://styles/margo4owlp/ckn933jwf0sw717qnbxenlsnf"
        center={userLocation}
        zoom={currentZoom}
        movingMethod="jumpTo"
        containerStyle={{
          display: 'flex',
          flex: 1,
        }}>
        <ZoomControl />
        <ScaleControl />
        <Cluster
          ClusterMarkerFactory={clusterMarker}
          zoomOnClick
          zoomOnClickPadding={100}>
          {animalList.map(
            ({ location: { coordinates = [] } = {}, id = '' }) => {
              return (
                <Marker
                  key={id}
                  coordinates={coordinates}
                  anchor="bottom"
                  style={{ cursor: 'pointer' }}
                  onClick={() => push(`/animal-details/${id}`)}>
                  <img style={{ height: 36 }} src={marker} />
                </Marker>
              );
            },
          )}
        </Cluster>
      </Map>
    </div>
  );
};
