import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useAnimalImages } from '../hooks/useAnimalImages';
import { ReactComponent as FavoriteInactive } from './../../images/favorite-inactive.svg';
import { ReactComponent as FavoriteActive } from './../../images/favorite-active.svg';
import { ReactComponent as GargabeCan } from './../../images/garbage-can.svg';
import { first, isEmpty } from 'lodash';
import { useHistory } from 'react-router';

export default ({ animalId, animal, updateAnimal }) => {
  const inputRef = useRef(null);
  const {
    uploadFile,
    deleteFile,
    progress,
    uploading,
    deleteList,
    images,
  } = useAnimalImages(animalId);
  const { t } = useTranslation();
  const { push } = useHistory();

  useEffect(() => {
    if (
      !isEmpty(images) &&
      !isEmpty(animal) &&
      (!animal?.primaryImage ||
        isEmpty(animal?.primaryImage) ||
        !images.find(({ id }) => id === animal?.primaryImage?.id))
    ) {
      updateAnimal({ primaryImage: first(images) });
    }
  }, [images, animal]);

  return (
    <>
      <button
        className="button-secondary"
        onClick={() => push(`/animal-details/${animalId}`)}>
        {t('done')}
      </button>
      <div className="upload-images-form">
        {images.map((file, index) => {
          const favorite = animal.primaryImage?.id === file.id;
          return (
            <div
              style={{ opacity: deleteList.includes(file.id) ? 0.5 : 1 }}
              key={file.id}
              className="image-item">
              <div className="image-container">
                <img src={file.uri} alt={`image-${index}`} />
              </div>
              <div className="options">
                {favorite ? (
                  <FavoriteActive />
                ) : (
                  <FavoriteInactive
                    onClick={() => updateAnimal({ primaryImage: file })}
                  />
                )}
                <GargabeCan
                  onClick={() => {
                    deleteFile(file, () => {
                      if (favorite) updateAnimal({ primaryImage: {} });
                    });
                  }}
                />
              </div>
            </div>
          );
        })}
        <div className={`upload-cell${uploading ? '-uploading' : ''}`}>
          <label htmlFor="upload-input">
            {!uploading && (
              <span className="title">{`${t('addPhoto')}...`}</span>
            )}
            <span className="status">
              {uploading ? `${Math.round(progress)}%` : '+'}
            </span>
          </label>
          <input
            type="file"
            accept="image/*"
            id="upload-input"
            ref={inputRef}
            disabled={uploading}
            onChange={({ target: { files = [] } }) => {
              const [file] = files;
              if (file) uploadFile(file, () => (inputRef.current.value = ''));
            }}
          />
        </div>
      </div>
    </>
  );
};
