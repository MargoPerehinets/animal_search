import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

export default () => {
  const { t } = useTranslation();
  const { push } = useHistory();

  return (
    <div className="profile-nav">
      <button
        className="button-secondary"
        onClick={() => push('/create-new-animal/missing')}>
        {t('reportMissing')}
      </button>
      <button
        className="button-secondary"
        onClick={() => push('/create-new-animal/found')}>
        {t('reportFound')}
      </button>
      <button
        className="button-secondary"
        onClick={() => push('/animal-list/missing')}>
        {t('watchMissing')}
      </button>
      <button
        className="button-secondary"
        onClick={() => push('/animal-list/found')}>
        {t('watchFound')}
      </button>
    </div>
  );
};
