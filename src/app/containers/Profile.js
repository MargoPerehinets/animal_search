import React, { useContext, useState } from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useUser } from '../context/user';

export default () => {
  const { user, updateUser } = useUser();
  const { push } = useHistory();
  const { t } = useTranslation();
  const [editing, setEditing] = useState(false);
  const [username, setUsername] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const apply =
    editing && (phoneNumber !== user.phoneNumber || username !== user.username);

  return (
    <div className="profile">
      <h1>{t('information')}</h1>
      {!editing && (
        <button
          className="button-secondary"
          onClick={async () => {
            await firebase.auth().signOut();
            push('/');
          }}>
          {t('logout')}
        </button>
      )}
      <div className="cell" style={{ opacity: editing ? 0.3 : 1 }}>
        <span className="label">{t('email')}</span>
        <span className="value">{user.email}</span>
      </div>
      <div className="cell">
        <span className="label">{t('name')}</span>
        {editing ? (
          <input
            type="text"
            className="edit-input"
            value={username}
            onChange={({ target: { value } }) => setUsername(value)}
          />
        ) : (
          <span className="value">{user.username}</span>
        )}
      </div>
      <div className="cell">
        <span className="label">{t('phoneNumber')}</span>
        {editing ? (
          <input
            type="text"
            className="edit-input"
            value={phoneNumber}
            onChange={({ target: { value } }) => setPhoneNumber(value)}
          />
        ) : (
          <span className="value">{user.phoneNumber}</span>
        )}
      </div>

      <div className="cell">
        <button
          className="button-secondary borderless"
          onClick={() => {
            setEditing((prevState) => !prevState);
            if (!editing) {
              setUsername(user.username);
              setPhoneNumber(user.phoneNumber);
            }
          }}>
          {t(editing ? 'cancel' : 'edit')}
        </button>
        {apply && (
          <button
            className="button-secondary borderless"
            onClick={() => {
              updateUser({ phoneNumber, username });
              setEditing(false);
            }}>
            {t('apply')}
          </button>
        )}
      </div>
    </div>
  );
};
