import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router';
import { InputSecondary } from '../components/InputSecondary';
import { Select } from '../components/Select';
import { TextAreaSecondary } from '../components/TextAreaSecondary';
import firebase from 'firebase/app';
import 'firebase/firestore';
import cuid from 'cuid';
import { useUser } from '../context/user';
import { ReactComponent as Loader } from './../../images/loader.svg';
import { isEmpty, sortBy } from 'lodash';
import { accessToken } from './../config/mapbox.json';
import { useDebounce } from '../hooks/useDebounce';
import PlacesAutocomplete, {
  geocodeByAddress,
  geocodeByPlaceId,
  getLatLng,
} from 'react-places-autocomplete';
import ReactMapboxGl from 'react-mapbox-gl';
import marker from './../../images/marker.png';
import { Marker } from 'react-mapbox-gl';
import { animalTypes } from '../modules/animal-types';

const Map = ReactMapboxGl({ accessToken });

export default () => {
  const { t } = useTranslation();
  const { reportType } = useParams();
  const { push } = useHistory();

  const { user, hasUser } = useUser();

  const [type, setType] = useState(null);
  const [breed, setBreed] = useState(null);
  const [gender, setGender] = useState(null);
  const [color, setColor] = useState(null);
  const [sterilization, setSterilization] = useState(null);
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [selectedAdress, selectAddress] = useState({});
  const [description, setDescription] = useState('');

  const [submiting, setSubmitStatus] = useState(false);

  const submitForm = useCallback(
    (ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      const submit = async () => {
        try {
          setSubmitStatus(true);
          if (!hasUser) throw new Error('User is missing');
          if (!type || !selectedAdress)
            throw new Error('Missing required params');
          const id = cuid();
          await firebase
            .firestore()
            .collection('animals')
            .doc(id)
            .set({
              id,
              name,
              type: type.value,
              breed: breed?.value || null,
              gender: gender?.value || null,
              color: color?.value || null,
              sterilization: sterilization?.value || null,
              location: selectedAdress,
              description,
              reportType,
              postedBy: user?.uid
            })
            .then(() => push(`/animal-upload-images/${id}`));
        } catch (err) {
          console.log(err);
          setSubmitStatus(false);
        }
      };
      submit();
    },
    [
      type,
      breed,
      gender,
      color,
      sterilization,
      name,
      selectedAdress,
      description,
      user,
      hasUser,
    ],
  );

  const handleSelectAddress = useCallback((address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then(({ lat, lng }) => selectAddress({ coordinates: [lng, lat], address }))
      .catch((error) => console.error('Error', error));
  }, []);

  useEffect(() => {
    setType(null);
    setBreed(null);
    setGender(null);
    setColor(null);
    setSterilization(null);
  }, [reportType]);

  const currentBreeds = useMemo(() => {
    return sortBy(
      type?.breeds.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentGenders = useMemo(() => {
    return sortBy(
      type?.genders.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentColors = useMemo(() => {
    return sortBy(
      type?.colors.map((it) => ({ value: it, label: t(it) })) || [],
      ({ label }) => label,
    );
  }, [type]);

  const currentSterilization = useMemo(() => {
    return (
      type?.sterilization.map((it) => ({
        value: it,
        label: it ? t('yes') : t('no'),
      })) || []
    );
  }, [type]);

  if (!['missing', 'found'].includes(reportType)) return null;
  // console.log(foundAdresses);
  return (
    <form className="animal-creation-form" onSubmit={submitForm}>
      <div className="left">
        <Select
          label={t('type')}
          // isDisabled
          isClearable
          isSearchable
          isRequired
          options={animalTypes.map((it) => ({ ...it, label: t(it.value) }))}
          value={type}
          onChange={(val) => {
            setType(val);
            if (!val || type?.value !== val?.value) {
              setBreed(null);
              setGender(null);
              setColor(null);
              setSterilization(null);
            }
          }}
        />
        <Select
          label={t('breed')}
          isDisabled={!type}
          isClearable
          isSearchable
          value={breed}
          options={currentBreeds}
          onChange={setBreed}
        />
        <Select
          label={t('gender')}
          isDisabled={!type}
          isClearable
          isSearchable
          value={gender}
          options={currentGenders}
          onChange={setGender}
        />
        <Select
          label={t('color')}
          isDisabled={!type}
          isClearable
          isSearchable
          value={color}
          options={currentColors}
          onChange={setColor}
        />
        <Select
          label={t('sterilization')}
          isDisabled={!type}
          isClearable
          isSearchable
          value={sterilization}
          options={currentSterilization}
          onChange={setSterilization}
        />
      </div>
      <div className="right">
        <InputSecondary
          value={name}
          onChange={setName}
          label={t('petName')}
          placeholder={t('defaultPetsName')}
        />
        <TextAreaSecondary
          value={description}
          onChange={setDescription}
          label={t('description')}
        />
        <PlacesAutocomplete
          value={address}
          onChange={setAddress}
          onSelect={handleSelectAddress}
          debounce={500}>
          {({
            getInputProps,
            suggestions,
            getSuggestionItemProps,
            loading,
          }) => {
            return (
              <div className="google-autocomplete">
                <InputSecondary
                  label={t('address')}
                  isRequired
                  id="google-autocomplete"
                  raw
                  {...getInputProps({})}
                />
                {isEmpty(suggestions) ? null : (
                  <div className="autocomplete-results">
                    {suggestions.map((suggestion, index) => {
                      return (
                        <div
                          key={index}
                          className={`autocomplete-item${
                            suggestion.active ? ' autocomplete-item-active' : ''
                          }`}
                          {...getSuggestionItemProps(suggestion)}
                          onClick={() => {
                            document
                              .getElementById('google-autocomplete')
                              .blur();
                            setAddress(suggestion.description);
                            handleSelectAddress(suggestion.description);
                          }}>
                          <span>{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            );
          }}
        </PlacesAutocomplete>
        {isEmpty(selectedAdress) ? null : (
          <Map
            style="mapbox://styles/margo4owlp/ckn933jwf0sw717qnbxenlsnf"
            center={selectedAdress.coordinates}
            zoom={[14]}
            movingMethod="jumpTo"
            containerStyle={{
              display: 'flex',
              flex: 1,
              minHeight: 300,
              marginTop: 24,
              borderRadius: 16,
            }}>
            <Marker
              coordinates={selectedAdress.coordinates}
              anchor="bottom"
              onClick={() => {}}>
              <img style={{ height: 36 }} src={marker} />
            </Marker>
          </Map>
        )}
        <div className="button-submit">
          <button
            style={{ minWidth: 180 }}
            type="submit"
            disabled={submiting}
            className={`button-secondary${submiting ? '-loading' : ''}`}>
            {submiting ? <Loader className="loader" /> : t('continue')}
          </button>
        </div>
      </div>
    </form>
  );
};
