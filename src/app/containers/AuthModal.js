import React, { useCallback, useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { InputPrimary } from '../components/InputPrimary';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { useUser } from '../context/user';

export default (props) => {
  const [authType, setAuthType] = useState('signin');
  const isSignin = authType === 'signin';

  if (isSignin) {
    return <SigninForm {...props} setAuthType={setAuthType} />;
  }
  return <SignupForm {...props} setAuthType={setAuthType} />;
};

const SigninForm = ({ close = () => {}, setAuthType = () => {} }) => {
  const { t } = useTranslation();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onSubmit = useCallback(
    (ev) => {
      ev.preventDefault();
      ev.stopPropagation();

      const signin = async () => {
        try {
          setErrorMessage('');
          setLoading(true);

          if (!password || !email) {
            setLoading(false);
            setErrorMessage(t('auth/missing-fields'));
            return;
          }

          await firebase
            .auth()
            .signInWithEmailAndPassword(email.toLowerCase(), password);
          close();
          setLoading(false);
        } catch (err) {
          setLoading(false);
          setErrorMessage(t(err.code));
        }
      };
      signin();
    },
    [email, password],
  );

  return (
    <div
      onClick={(ev) => {
        if (loading) return;
        if (ev.target.className === 'signin-form') close();
      }}
      className="signin-form">
      <form className="container" onSubmit={onSubmit}>
        <h1>{t('signin')}</h1>
        <InputPrimary value={email} onChange={setEmail} label={t('email')} />
        <InputPrimary
          value={password}
          onChange={setPassword}
          label={t('password')}
          type="password"
        />
        <button type="submit" className="button-primary">
          {t('signin')}
        </button>
        {loading && <p className="success-message">{t('loading')}</p>}
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        <div className="option">
          <p>{t('dontHaveAccount')}</p>
          <button
            className="button-secondary"
            onClick={() => setAuthType('signup')}>
            {t('signup')}
          </button>
        </div>
      </form>
    </div>
  );
};

const SignupForm = ({ close = () => {}, setAuthType = () => {} }) => {
  const { t } = useTranslation();
  const { fetchUser } = useUser();

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');

  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onSubmit = useCallback(
    (ev) => {
      ev.preventDefault();
      ev.stopPropagation();

      const signup = async () => {
        try {
          setErrorMessage('');
          setLoading(true);

          if (!password || !username || !email || !repeatPassword) {
            setLoading(false);
            setErrorMessage(t('auth/missing-fields'));
            return;
          }

          if (password !== repeatPassword) {
            setLoading(false);
            setErrorMessage(t('auth/password-not-match'));
            return;
          }

          const {
            user: { uid } = {},
          } = await firebase
            .auth()
            .createUserWithEmailAndPassword(email, password);
          await firebase
            .firestore()
            .collection('users')
            .doc(uid)
            .set({ username, email, uid, phoneNumber })
            .then(() => {});
          await fetchUser(uid);

          setLoading(false);
          close();
        } catch (err) {
          setLoading(false);
          setErrorMessage(t(err.code));
        }
      };
      signup();
    },
    [username, email, password, repeatPassword, phoneNumber],
  );

  return (
    <div
      onClick={(ev) => {
        if (loading) return;
        if (ev.target.className === 'signup-form') close();
      }}
      className="signup-form">
      <form className="container" onSubmit={onSubmit}>
        <h1>{t('signup')}</h1>
        <InputPrimary
          value={username}
          onChange={setUsername}
          label={t('username')}
        />
        <InputPrimary value={email} onChange={setEmail} label={t('email')} />
        <InputPrimary
          value={phoneNumber}
          onChange={setPhoneNumber}
          label={`${t('phoneNumber')} (${t('optional')})`}
        />
        <InputPrimary
          value={password}
          onChange={setPassword}
          label={t('password')}
          type="password"
        />
        <InputPrimary
          value={repeatPassword}
          onChange={setRepeatPassword}
          label={t('confirmPassword')}
          type="password"
        />
        <button type="submit" className="button-primary">
          {t('signup')}
        </button>
        {loading && <p className="success-message">{t('loading')}</p>}
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        <div className="option">
          <p>{t('alreadyHaveAccount')}</p>
          <button
            className="button-secondary"
            onClick={() => setAuthType('signin')}>
            {t('signin')}
          </button>
        </div>
      </form>
    </div>
  );
};
