import { isEmpty } from 'lodash';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useParams } from 'react-router';
import { useUser } from '../context/user';
import { useAnimal } from '../hooks/useAnimal';
import { useAnimalImages } from '../hooks/useAnimalImages';
import { useExternalUser } from '../hooks/useExternalUser';
import { ReactComponent as Loader } from './../../images/loader.svg';
import { ReactComponent as Mail } from './../../images/mail.svg';
import firebase from 'firebase/app';
import 'firebase/firestore';

export default () => {
  const { animalId } = useParams();
  const { user } = useUser();
  const { t } = useTranslation();
  const { push } = useHistory();
  const [deleting, setDeleting] = useState(false);
  const {
    animal,
    animal: {
      type,
      reportType,
      description,
      breed,
      color,
      gender,
      sterilization,
      name,
      postedBy,
      primaryImage = {},
      location: { address } = {},
    },
    fetching: fetchingAnimal,
  } = useAnimal(animalId);

  const { images: animalPhotos } = useAnimalImages(animalId);

  const { user: externalUser } = useExternalUser(postedBy);

  const isMe = user.uid === animal.postedBy;

  const sortedAnimalPhotos = useMemo(
    () => animalPhotos.sort((a, b) => a.id === primaryImage?.id),
    [animalPhotos, primaryImage],
  );

  const deleteAnimal = useCallback(() => {
    const remove = async () => {
      try {
        setDeleting(true);
        await firebase
          .firestore()
          .collection('animals')
          .doc(animalId)
          .delete()
          .then(() => {
            push('/profile');
            setDeleting(false);
          });
      } catch (err) {
        setDeleting(false);
        console.log('Error deleting animal', err);
      }
    };
    remove();
  }, [animalId]);

  if (fetchingAnimal)
    return (
      <div className="loader-wrap">
        <Loader className="loader" />
      </div>
    );
  if (isEmpty(animal)) return <h1>{t('suchAnimalNotExist')}</h1>;

  return (
    <div className="animal-details">
      <div className="top">
        <div className="left">
          {isMe && (
            <button
              className={`button-secondary${deleting ? '-loading' : ''}`}
              onClick={deleteAnimal}>
              {deleting ? <Loader className="loader" /> : t('delete')}
            </button>
          )}
          <h1>
            {`${t(`${reportType}WithType`, { type: t(type).toLowerCase() })}`}
            {name ? <span>{` "${name}"`}</span> : ''}
          </h1>
          <p className="description">{` ${t('atWithAddress', {
            address,
          })}. ${description}`}</p>
        </div>
        <div className="right">
          {isMe && (
            <button
              className="button-secondary"
              onClick={() => push(`/animal-upload-images/${animalId}`)}>
              {t('editPhotos')}
            </button>
          )}
          <div className="animal-photos">
            {sortedAnimalPhotos.map(({ id, uri, type }, index) => {
              return (
                <div key={id} className="image-item">
                  <img src={uri} alt={`image-${index}`} />
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="bot">
        <div className="bot-wrap">
          <div className="left">
            <div className="box-cell">
              <span className="label">{t('type')}</span>
              <span className="value">{t(type)}</span>
            </div>
            <div className="box-cell">
              <span className="label">{t('gender')}</span>
              <span className="value">{gender ? t(gender) : '-'}</span>
            </div>
            <div className="box-cell">
              <span className="label">{t('sterilization')}</span>
              <span className="value">
                {typeof sterilization === 'boolean'
                  ? sterilization
                    ? t('yes')
                    : t('no')
                  : '-'}
              </span>
            </div>
            <div className="box-cell">
              <span className="label">{t('breed')}</span>
              <span className="value">{breed ? t(breed) : '-'}</span>
            </div>
            <div className="box-cell">
              <span className="label">{t('color')}</span>
              <span className="value">{color ? t(color) : '-'}</span>
            </div>
          </div>
          <div className="right">
            <span className="title">
              {t('contact')}
              <Mail />
            </span>
            <div className="cell">
              <span className="label">{t('name')}</span>
              <span className="value">{externalUser.username}</span>
            </div>
            <div className="cell">
              <span className="label">{t('email')}</span>
              <span className="value">{externalUser.email}</span>
            </div>
            <div className="cell">
              <span className="label">{t('phoneNumber')}</span>
              <span className="value">{externalUser.phoneNumber}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
