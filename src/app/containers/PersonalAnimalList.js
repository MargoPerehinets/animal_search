import React, { useEffect, useState } from 'react';
import cuid from 'cuid';
import { useTranslation } from 'react-i18next';
import { useUser } from '../context/user';
import firebase from 'firebase/app';
import 'firebase/firestore';
import { AnimalAvatar } from '../components/AnimalAvatar';

export default ({ type = 'missing' }) => {
  const { t } = useTranslation();
  const missing = type === 'missing';
  const { user, hasUser } = useUser();

  const [animalList, setAnimalList] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchAnimals = async () => {
      try {
        await firebase
          .firestore()
          .collection('animals')
          .where('postedBy', '==', user.uid)
          .where('reportType', '==', type)
          .limit(9)
          .get()
          .then((qs) => {
            const data = [];
            qs.forEach((doc) => data.push(doc.data()));
            setAnimalList(data);
          });
        setLoading(false);
      } catch (err) {
        setLoading(false);
        console.log(err);
      }
    };
    if (hasUser) fetchAnimals();
  }, [user, hasUser]);

  console.log(animalList);

  return (
    <div className="personal-animal-list">
      <h1>{t(missing ? 'missing' : 'found')}</h1>
      <div className="animal-list">
        {animalList.map(({ id, name, type, primaryImage = {} }, index) => {
          return <AnimalAvatar uri={primaryImage.uri} key={id} type={type} id={id} name={name} />;
        })}
      </div>
    </div>
  );
};
