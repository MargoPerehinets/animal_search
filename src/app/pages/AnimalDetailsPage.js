import React from 'react';
import { useParams } from 'react-router';
import AnimalDetails from '../containers/AnimalDetails';

export default ({ ...props }) => {
  return (
    <section className="animal-details-page">
      <AnimalDetails />
    </section>
  );
};
