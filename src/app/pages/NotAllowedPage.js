import React from 'react';
import { useTranslation } from 'react-i18next';
import { ReactComponent as NoResults } from './../../images/no-results.svg';

export default () => {
  const { t } = useTranslation();
  return (
    <section className="not-allowed-page">
      <h1>{t('notAllowed')}</h1>
      <NoResults />
    </section>
  );
};
