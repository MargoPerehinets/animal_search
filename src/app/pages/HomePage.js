import React from 'react';
import { ReactComponent as CatBig } from './../../images/cat-illustration.svg';

export default () => (
  <section className="home-page">
    <CatBig />
  </section>
);
