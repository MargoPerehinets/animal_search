import React from 'react';
import { useTranslation } from 'react-i18next';
import { ReactComponent as NoResults } from './../../images/no-results.svg';

export default () => {
  const { t } = useTranslation();
  return (
    <section className="not-found-page">
      <h1>{t('pageNotFound')}</h1>
      <NoResults />
    </section>
  );
};
