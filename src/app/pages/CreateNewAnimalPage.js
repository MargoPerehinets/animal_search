import React from 'react';
import AnimalCreationForm from '../containers/AnimalCreationForm';

export default () => (
  <section className="create-new-animal-page">
      <AnimalCreationForm />
  </section>
);
