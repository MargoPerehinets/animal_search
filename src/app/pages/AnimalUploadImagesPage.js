import { isEmpty } from 'lodash';
import React from 'react';
import { useParams } from 'react-router';
import AnimalUploadImagesForm from '../containers/AnimalUploadImagesForm';
import { useUser } from '../context/user';
import { useAnimal } from '../hooks/useAnimal';
import NotAllowedPage from './NotAllowedPage';
import { ReactComponent as Loader } from './../../images/loader.svg';
import NotFoundPage from './NotFoundPage';

export default () => {
  const { user } = useUser();
  const { animalId } = useParams();
  const { animal, fetching: fetchingAnimal, updateAnimal } = useAnimal(
    animalId,
  );

  if (fetchingAnimal)
    return (
      <section className="animal-upload-images">
        <div className="loader-wrap">
          <Loader className="loader" />
        </div>
      </section>
    );

  if (isEmpty(animal) || animal.postedBy !== user.uid)
    return <NotFoundPage />;

  return (
    <section className="animal-upload-images">
      <AnimalUploadImagesForm
        animalId={animalId}
        animal={animal}
        updateAnimal={updateAnimal}
      />
    </section>
  );
};
