import React from 'react';
import Map from '../containers/Map';

export default () => (
  <section className="animal-list-page">
    <Map />
  </section>
);
