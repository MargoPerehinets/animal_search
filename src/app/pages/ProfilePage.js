import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import PersonalAnimalList from '../containers/PersonalAnimalList';
import Profile from '../containers/Profile';
import ProfileNav from '../containers/ProfileNav';

export default () => {
  return (
    <section className="profile-page">
      <ProfileNav />
      <div className="profile-details">
        <Profile />
        <PersonalAnimalList />
        <PersonalAnimalList type="found" />
      </div>
    </section>
  );
};
